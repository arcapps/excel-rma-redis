import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisCacheService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  public async del(provideKey: string): Promise<void> {
    const keys = await this.keys();
    if (keys.includes(provideKey)) await this.cacheManager.del(provideKey);
  }

  public async keys() {
    const allKeys = await this.cacheManager.store.keys('*');
    return allKeys || [];
  }

  public async clear() {
    await this.cacheManager.store.reset();
  }

  public async invalidateListCache(provideKey: string) {
    const keys = await this.keys();
    if (keys.length !== 0) {
      for (const key of keys) {
        if (key.includes(provideKey)) {
          await this.del(key);
        }
      }
    }
  }
}
