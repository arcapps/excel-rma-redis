import { CacheModule, Module } from '@nestjs/common';
// import { Cache } from 'cache-manager';
import * as redisStore from 'cache-manager-ioredis';
import { RedisCacheService } from './redis.service';
import {
  ConfigService,
  REDIS_HOST,
  REDIS_PORT,
} from '../config/config.service';

@Module({
  imports: [
    CacheModule.registerAsync({
      useFactory: () => {
        return {
          store: redisStore,
          host: new ConfigService().get(REDIS_HOST),
          port: new ConfigService().get(REDIS_PORT),
        };
      },
    }),
  ],
  providers: [RedisCacheService],
  exports: [RedisCacheModule, RedisCacheService],
})
export class RedisCacheModule {}
