import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';

import { SalesInvoiceWebhookAggregateService } from '../../aggregates/sales-invoice-webhook-aggregate/sales-invoice-webhook-aggregate.service';
import { SalesInvoiceWebhookDto } from '../../entity/sales-invoice/sales-invoice-webhook-dto';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('sales_invoice')
export class SalesInvoiceWebhookController {
  constructor(
    private readonly salesInvoiceWebhookAggregate: SalesInvoiceWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCreated(
    @Body() purchaseInvoicePayload: SalesInvoiceWebhookDto,
  ) {
    // ! custom invalidate cache
    await this.redisService.invalidateListCache('/api/sales_invoice/v1/list');

    if (purchaseInvoicePayload.is_return) return;

    return this.salesInvoiceWebhookAggregate.salesInvoiceCreated(
      purchaseInvoicePayload,
    );
  }

  @Post('webhook/v1/cancel')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCanceled(
    @Body() purchaseInvoicePayload: { name: string },
  ) {
    // ! custom invalidate cache
    await this.redisService.invalidateListCache('/api/sales_invoice/v1/list');
    await this.redisService.invalidateListCache('/api/sales_invoice/v1/get');

    return this.salesInvoiceWebhookAggregate.salesInvoiceCanceled(
      purchaseInvoicePayload,
    );
  }
}
