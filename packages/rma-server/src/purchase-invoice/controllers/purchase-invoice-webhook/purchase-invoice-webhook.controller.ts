import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
/* eslint-disable */
import { PurchaseInvoiceWebhookDto } from '../../entity/purchase-invoice/purchase-invoice-webhook-dto';
import { PurchaseInvoiceWebhookAggregateService } from '../../aggregates/purchase-invoice-webhook-aggregate/purchase-invoice-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';
import { RedisCacheService } from '../../../redis/redis.service';

/* eslint-enable */

@Controller('purchase_invoice')
export class PurchaseInvoiceWebhookController {
  constructor(
    private readonly purchaseInvoiceWebhookAggregate: PurchaseInvoiceWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCreated(
    @Body() purchaseInvoicePayload: PurchaseInvoiceWebhookDto,
  ) {
    // ! invalidate cache
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/list',
    );

    // ! create purchase invoice
    return this.purchaseInvoiceWebhookAggregate.purchaseInvoiceCreated(
      purchaseInvoicePayload,
    );
  }

  @Post('webhook/v1/cancel')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCancelled(@Body('name') name: string) {
    // ! invalidate cache
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/list',
    );
    await this.redisService.invalidateListCache('/api/purchase_invoice/v1/get');

    // ! cancel purchase invoice
    return this.purchaseInvoiceWebhookAggregate.cancelPurchaseInvoice(name);
  }
}
