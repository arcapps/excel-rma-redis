import {
  Controller,
  Post,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { RetrieveSupplierQuery } from '../../query/get-supplier/retrieve-supplier.query';
import { RetrieveSupplierListQuery } from '../../query/list-supplier/retrieve-supplier-list.query';
import { SupplierDto } from '../../entity/supplier/supplier-dto';
import { AddSupplierCommand } from '../../command/add-supplier/add-supplier.command';
import { RemoveSupplierCommand } from '../../command/remove-supplier/remove-supplier.command';
import { UpdateSupplierCommand } from '../../command/update-supplier/update-supplier.command';
import { UpdateSupplierDto } from '../../entity/supplier/update-supplier-dto';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('supplier')
export class SupplierController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async create(@Body() supplierPayload: SupplierDto, @Req() req) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('api/supplier/v1/list');

    return this.commandBus.execute(
      new AddSupplierCommand(supplierPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  async remove(@Param('uuid') uuid) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('api/supplier/v1/list');

    // ! invalidate the specific cache
    await this.redisService.del(`api/supplier/v1/get/${uuid}`);

    // finally, execute the command
    return this.commandBus.execute(new RemoveSupplierCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getSupplier(@Param('uuid') uuid, @Req() req) {
    return await this.queryBus.execute(new RetrieveSupplierQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getSupplierList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveSupplierListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateSupplier(@Body() updatePayload: UpdateSupplierDto) {
    // ! as i don't whiche cach is updated, i will invalidate all and also the list cache
    await this.redisService.invalidateListCache('api/supplier/v1/list');
    await this.redisService.invalidateListCache('api/supplier/v1/get');

    return this.commandBus.execute(new UpdateSupplierCommand(updatePayload));
  }
}
