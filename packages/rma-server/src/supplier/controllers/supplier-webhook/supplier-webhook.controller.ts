import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { SupplierWebhookInterface } from '../../entity/supplier/supplier-webhook-interface';
import { SupplierWebhookAggregateService } from '../../aggregates/supplier-webhook-aggregate/supplier-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('supplier')
export class SupplierWebhookController {
  constructor(
    private readonly supplierWebhookAggregate: SupplierWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async SupplierCreated(@Body() supplierPayload: SupplierWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('api/supplier/v1/list');

    return this.supplierWebhookAggregate.supplierCreated(supplierPayload);
  }

  @Post('webhook/v1/update')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async SupplierUpdated(@Body() supplierPayload: SupplierWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('api/supplier/v1/list');
    await this.redisService.invalidateListCache('api/supplier/v1/get');

    return this.supplierWebhookAggregate.supplierUpdated(supplierPayload);
  }

  @Post('webhook/v1/delete')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  SupplierDeleted(@Body() supplierPayload: SupplierWebhookInterface) {
    // ! invalidate list cache
    this.redisService.invalidateListCache('api/supplier/v1/list');
    this.redisService.invalidateListCache('api/supplier/v1/get');

    return this.supplierWebhookAggregate.supplierDeleted(supplierPayload);
  }
}
