import { CacheTTL, Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @CacheTTL(1)
  getRoot() {
    return this.appService.getRoot();
  }

  @Get('info')
  @CacheTTL(1)
  async info() {
    return this.appService.info();
  }
}
