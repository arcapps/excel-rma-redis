import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Body,
} from '@nestjs/common';
/* eslint-disable */
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';

import { DeliveryNoteWebhookDto } from '../../../delivery-note/entity/delivery-note-service/delivery-note-webhook.dto';
import { DeliveryNoteWebhookAggregateService } from '../../../delivery-note/aggregates/delivery-note-webhook-aggregate/delivery-note-webhook-aggregate.service';
import { RedisCacheService } from '../../../redis/redis.service';
/* eslint-enable */

@Controller('delivery_note')
export class DeliveryNoteWebhookController {
  constructor(
    private readonly deliveryNoteAggregateService: DeliveryNoteWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async createdDeliveryNote(
    @Body() deliveryNotePayload: DeliveryNoteWebhookDto,
  ) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/delivery_note/v1/list');
    await this.redisService.invalidateListCache(
      '/api/delivery_note/v1/relay_list_warehouses',
    );

    return this.deliveryNoteAggregateService.createdDeliveryNote(
      deliveryNotePayload,
    );
  }

  @Post('webhook/v1/update')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updatedDeliveryNote(
    @Body() deliveryNotePayload: DeliveryNoteWebhookDto,
  ) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/delivery_note/v1/list');
    await this.redisService.invalidateListCache(
      '/api/delivery_note/v1/relay_list_warehouses',
    );

    return 'update coming soon';
  }

  @Post('webhook/v1/delete')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async deletedDeliveryNote(
    @Body() deliveryNotePayload: DeliveryNoteWebhookDto,
  ) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/delivery_note/v1/list');
    await this.redisService.invalidateListCache(
      '/api/delivery_note/v1/relay_list_warehouses',
    );

    return 'coming soon';
  }
}
