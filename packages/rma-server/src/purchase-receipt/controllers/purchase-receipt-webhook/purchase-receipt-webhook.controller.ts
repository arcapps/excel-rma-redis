import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Body,
} from '@nestjs/common';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';

import { PurchaseReceiptAggregateService } from '../../aggregates/purchase-receipt-aggregate/purchase-receipt-aggregate.service';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('purchase_receipt')
export class PurchaseReceiptWebhookController {
  constructor(
    private readonly purchaseReceiptAggregate: PurchaseReceiptAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}
  @Post('webhook/v1/cancel')
  @UsePipes(ValidationPipe)
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCreated(@Body() purchaseReceiptPayload) {
    // invalidate cache
    await this.redisService.invalidateListCache(
      '/api/stock_ledger/v1/list_stock_ledger',
    );
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/list',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_order/v1/get_po_from_pi_number',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/get/',
    );
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/stock_entry/v1/list');
    await this.redisService.invalidateListCache('/api/stock_entry/v1/get');
    await this.redisService.invalidateListCache('/api/serial_no/v1/list');
    await this.redisService.invalidateListCache(
      '/api/serial_no/v1/get_history',
    );

    // finally call the aggregate service
    return await this.purchaseReceiptAggregate.purchaseReceiptCancelled(
      purchaseReceiptPayload,
    );
  }
}
