import {
  Controller,
  Post,
  UseGuards,
  Req,
  UsePipes,
  ValidationPipe,
  Body,
  UseInterceptors,
  UploadedFile,
  BadRequestException,
} from '@nestjs/common';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { PurchaseReceiptDto } from '../../entity/purchase-receipt-dto';
import { PurchaseReceiptAggregateService } from '../../aggregates/purchase-receipt-aggregate/purchase-receipt-aggregate.service';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  APPLICATION_JSON_CONTENT_TYPE,
  INVALID_FILE,
} from '../../../constants/app-strings';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('purchase_receipt')
export class PurchaseReceiptController {
  constructor(
    private readonly purchaseReceiptAggregateService: PurchaseReceiptAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseInterceptors(FileInterceptor('file'))
  async create(
    @Body() purchaseReceiptPayload: PurchaseReceiptDto,
    @UploadedFile('file') file,
    @Req() req,
  ) {
    // invalidate cache
    await this.redisService.invalidateListCache(
      '/api/stock_ledger/v1/list_stock_ledger',
    );
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/list',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_order/v1/get_po_from_pi_number',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/get/',
    );
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/stock_entry/v1/list');
    await this.redisService.invalidateListCache('/api/stock_entry/v1/get');
    await this.redisService.invalidateListCache('/api/serial_no/v1/list');
    await this.redisService.invalidateListCache(
      '/api/serial_no/v1/get_history',
    );

    // finally call the responce
    if (file) {
      if (file.mimetype !== APPLICATION_JSON_CONTENT_TYPE) {
        throw new BadRequestException(INVALID_FILE);
      }
      return this.purchaseReceiptAggregateService.purchaseReceiptFromFile(
        file,
        req,
      );
    }
    return this.purchaseReceiptAggregateService.addPurchaseReceipt(
      purchaseReceiptPayload,
      req,
    );
  }

  @Post('v1/create_bulk')
  @UseGuards(TokenGuard)
  @UseInterceptors(FileInterceptor('file'))
  async createBulk(@UploadedFile('file') file, @Req() req) {
    // invalidate cache
    await this.redisService.invalidateListCache(
      '/api/stock_ledger/v1/list_stock_ledger',
    );
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/list',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_order/v1/get_po_from_pi_number',
    );
    await this.redisService.invalidateListCache(
      '/api/purchase_invoice/v1/get/',
    );
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/stock_entry/v1/list');
    await this.redisService.invalidateListCache('/api/stock_entry/v1/get');
    await this.redisService.invalidateListCache('/api/serial_no/v1/list');
    await this.redisService.invalidateListCache(
      '/api/serial_no/v1/get_history',
    );

    // finally call the aggregate service
    if (file && file.mimetype !== APPLICATION_JSON_CONTENT_TYPE) {
      throw new BadRequestException(INVALID_FILE);
    }
    return this.purchaseReceiptAggregateService.purchaseReceiptFromFile(
      file,
      req,
    );
  }
}
