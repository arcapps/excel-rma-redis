import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { CustomerWebhookDto } from '../../entity/customer/customer-webhook-interface';
import { CustomerWebhookAggregateService } from '../../aggregates/customer-webhook-aggregate/customer-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('customer')
export class CustomerWebhookController {
  constructor(
    private readonly customerWebhookAggregate: CustomerWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async customerCreated(@Body() customerPayload: CustomerWebhookDto) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/customer/v1/list');
    await this.redisService.invalidateListCache('/api/customer/v1/get');

    return this.customerWebhookAggregate.customerCreated(customerPayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async customerUpdated(@Body() customerPayload: CustomerWebhookDto) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/customer/v1/list');
    await this.redisService.invalidateListCache('/api/customer/v1/get');

    return this.customerWebhookAggregate.customerUpdated(customerPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async customerDeleted(@Body() customerPayload: CustomerWebhookDto) {
    // ! invalidate the list cache
    await this.redisService.invalidateListCache('/api/customer/v1/list');
    await this.redisService.invalidateListCache('/api/customer/v1/get');

    return this.customerWebhookAggregate.customerDeleted(customerPayload);
  }
}
