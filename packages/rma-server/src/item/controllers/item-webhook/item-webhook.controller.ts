import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import {
  ItemBundleWebhookInterface,
  ItemWebhookInterface,
} from '../../entity/item/item-webhook-interface';
import { ItemWebhookAggregateService } from '../../aggregates/item-webhook-aggregate/item-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('item')
export class ItemWebhookController {
  constructor(
    private readonly itemWebhookAggregate: ItemWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async itemCreated(@Body() itemPayload: ItemWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/item/v1/get_by_names');
    await this.redisService.invalidateListCache('/api/item/v1/brand_list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_bundle_items',
    );

    // finaly create item
    return this.itemWebhookAggregate.itemCreated(itemPayload);
  }

  @Post('webhook/v1/bundle')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async bundleUpdated(@Body() bundlePayload: ItemBundleWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/item/v1/get_by_names');
    await this.redisService.invalidateListCache('/api/item/v1/brand_list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_bundle_items',
    );

    // finaly update bundle
    return this.itemWebhookAggregate.bundleUpdated(bundlePayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async itemUpdated(@Body() itemPayload: ItemWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/item/v1/get_by_names');
    await this.redisService.invalidateListCache('/api/item/v1/brand_list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_bundle_items',
    );

    // ! update item
    return this.itemWebhookAggregate.itemUpdated(itemPayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async itemDeleted(@Body() itemPayload: ItemWebhookInterface) {
    // ! invalidate list cache
    await this.redisService.invalidateListCache('/api/item/v1/list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_by_item_code',
    );
    await this.redisService.invalidateListCache('/api/item/v1/get_by_names');
    await this.redisService.invalidateListCache('/api/item/v1/brand_list');
    await this.redisService.invalidateListCache(
      '/api/item/v1/get_bundle_items',
    );

    // ! delete item
    return this.itemWebhookAggregate.itemDeleted(itemPayload);
  }
}
