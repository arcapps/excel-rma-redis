import { Module } from '@nestjs/common';
import { ProblemController } from './controllers/problem/problem.controller';
import { ProblemEntitiesModule } from './entity/entity.module';
import { ProblemAggregatesManager } from './aggregates';
import { RedisCacheModule } from '../redis/redis.module';

@Module({
  imports: [ProblemEntitiesModule, RedisCacheModule],
  controllers: [ProblemController],
  providers: [...ProblemAggregatesManager],
  exports: [ProblemEntitiesModule],
})
export class ProblemModule {}
