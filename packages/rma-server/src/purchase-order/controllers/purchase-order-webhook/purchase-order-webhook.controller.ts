import {
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  UseGuards,
  Body,
} from '@nestjs/common';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';
import { FrappeWebhookPipe } from '../../../auth/guards/webhook.pipe';

import { PurchaseOrderWebhookAggregateService } from '../../aggregates/purchase-order-webhook-aggregate/purchase-order-webhook-aggregate.service';
import { PurchaseOrderWebhookDto } from '../../entity/purchase-order/purchase-order-webhook-dto';
import { RedisCacheService } from '../../../redis/redis.service';

@Controller('purchase_order')
export class PurchaseOrderWebhookController {
  constructor(
    private readonly purchaseOrderWebhookAggregate: PurchaseOrderWebhookAggregateService,
    private readonly redisService: RedisCacheService,
  ) {}

  @Post('webhook/v1/create')
  @UsePipes(ValidationPipe)
  @UseGuards(FrappeWebhookGuard, FrappeWebhookPipe)
  async purchaseInvoiceCreated(
    @Body() purchaseOrderPayload: PurchaseOrderWebhookDto,
  ) {
    // ! invalidate cache
    await this.redisService.invalidateListCache('/api/purchase_order/v1/list');

    return this.purchaseOrderWebhookAggregate.purchaseOrderCreated(
      purchaseOrderPayload,
    );
  }
}
