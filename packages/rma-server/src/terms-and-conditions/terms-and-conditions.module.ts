import { Module } from '@nestjs/common';
import { TermsAndConditionsAggregateService } from './aggregates/terms-and-conditions-aggregate/terms-and-conditions-aggregate.service';
import { TermsAndConditionsController } from './controllers/terms-and-conditions/terms-and-conditions.controller';
import { TermsAndConditionsEntitiesModule } from './entity/entity.module';
import { RedisCacheModule } from '../redis/redis.module';

@Module({
  imports: [RedisCacheModule, TermsAndConditionsEntitiesModule],
  providers: [TermsAndConditionsAggregateService],
  controllers: [TermsAndConditionsController],
})
export class TermsAndConditionsModule {}
